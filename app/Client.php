<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Client extends Model
{
   	
	protected function whoIsVk($userId)
	{

		return json_decode(
					file_get_contents("https://api.vk.com/method/users.get?user_ids={$userId}&v=5.0")
				);

	}

	public function signUp($from, $soc_id, $first_name, $last_name)
	{

		$this->from = $from;
		$this->soc_id = $soc_id;
		$this->first_name = $first_name;
		$this->last_name = $last_name;
		$this->save();
	}

}
