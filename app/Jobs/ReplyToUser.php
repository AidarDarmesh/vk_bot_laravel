<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Client;
use App\Order;
use Illuminate\Support\Facades\DB;

class ReplyToUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $reply;

    protected function pagesNumberPdf($file)
    {
        // Выполняю скрипт и делаю парсинг на то, что выйдет в консоли
        // Никаких знаков в Windows и обязательный ./ в Linux
        exec("pdfinfo.exe {$file}", $output);

        // Бегаем по строкам
        $pagecount = 0;

        foreach($output as $op){
            // Вытаскиваем число
            if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1){
                // Кол-во страниц
                $pagecount = intval($matches[1]);
                break;
            }
        }

        return $pagecount;
    }

    public function __construct($data)
    {
        // Чтобы запросы мне мог делать только ВК
        if( $data->secret === config("vk.secret") )
        {
            $this->userId = $data->object->user_id;

            // Проверяем, был ли юзер до этого зарегистрирован у нас
            $count = Client::where('soc_id', $this->userId)->count();

            // Если юзера нет в нашей системе
            if( $count === 0 )
            {
                // Запрашиваем инфу о нем
                $about = Client::whoIsVk($this->userId);

                // Регистрируем
                
                $client->signUp(
                                "vk",
                                $this->userId,
                                $about->response[0]->first_name,
                                $about->response[0]->last_name
                            );

                $this->reply = "Привет, {$about->response[0]->first_name} 🖐🏻 " . 
                         "Вышли мне только ОДИН файл PDF";

                return;

            } else {
                // Проверка на спам файлами, 1 заказ в 2 минуты
                $order = Order::where("client_id", $this->userId);

                $var_dump($order);

                // В случае, если клиент уже есть в системе
                // Проверяем, существуют ли вложенности
                if( property_exists($data->object, "attachments") ){

                    // Считаю кол-во элементов во вложенности
                    $numb_elems = count($data->object->attachments);

                    // Мне нужен только 1 элемент
                    if( $numb_elems == 1 )
                    {
                        // Делаю проверку на тип вложенности этого элемента
                        // мне нужен только doc
                        if( property_exists($data->object->attachments[0], "doc") )
                        {

                            // Проверяю тип документа, беру только pdf
                            $ext = $data->object->attachments[0]->doc->ext;

                            if( $ext == "pdf" )
                            {
                                // Ссылка на файл
                                $url = $data->object->attachments[0]->doc->url;

                                // Оригинальное имя файла
                                $file_name = $data->object->attachments[0]->doc->title;

                                // // Хэш имя файла
                                // do{
                                //     // Создаем номер заказа
                                //     $numb = rand(100000, 999999);

                                // } while( Order::where('numb', $numb)->count() !== 0 );

                                $numb = rand(100000, 999999);

                                // Имя загрузочного файла
                                $hash_name = $numb . "." .  $ext;

                                // Если файл был загружен на сервер
                                if( copy($url, $hash_name) )
                                {
                                    // Если файл был загружен, применяю к нему скрипт подсчета страниц
                                    $pages = $this->pagesNumberPdf($hash_name);

                                    // Удаляю файл, чтобы не занимать много места на сервере
                                    unlink($hash_name);

                                    // Запись в заказа в БД
                                    $order = new Order();
                                    $order->client_id = ;
                                    $order->numb = $numb;
                                    $order->file_name = $file_name;
                                    $order->url = $data->object->attachments[0]->doc->url;
                                    $order->access_key = $data->object->attachments[0]->doc->access_key;
                                    $order->pages = $pages;
                                    $order->save();

                                    $numb_first = floor($numb/1000);
                                    $numb_second = $numb%1000;

                                    $this->reply =  "Номер заказа {$numb_first} {$numb_second}\n" . 
                                                    "В док-те {$file_name} {$pages} страниц\n" . 
                                                    "ВНИМАНИЕ: Заказ удалится через сутки, если не будет распечатан";

                                }

                            } else {
                                $this->reply = "Вы прислали файл НЕ PDF, пришлите PDF";
                            }

                        } else {
                            $this->reply = "Ваш файл не является документом";
                        }

                    } else {
                        $this->reply = "Вы прислали НЕСКОЛЬКО файлов, а нужно ОДИН";
                    }

                } else {
                    $this->reply = "Вы НЕ прислали файл";
                }
            }
        } else {
            echo "wrong secret";
        }

    }

    protected function reply()
    {
        // С помощью messages.send и токена сообщества отправляем ответное сообщение
        $request_params = array(
            "message" => $this->reply,
            "user_id" => $this->userId,
            "access_token" => config("vk.token"),
            "v" => config("vk.v")
        );

        $http_params = http_build_query($request_params);

        file_get_contents("https://api.vk.com/method/messages.send?" . $http_params);
    }

    public function handle()
    {

        $this->reply();

    }
}
