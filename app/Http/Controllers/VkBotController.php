<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\JustJob;
use App\Jobs\ReplyToUser;
use App\Client;
use App\Order;

class VkBotController extends Controller
{
	public function index($id)
	{
		return Order::where("client_id", $id)->last();
	}

	private function confirm()
	{
		return config('vk.confkey');
	}

	private function pagesNumber($file)
	{
		exec("pdfinfo {$file}", $output);

	    // Итерация по линиям
	    $pagecount = 0;

	    // Проходит по всем строкам
	    foreach($output as $op){

	        // Вытаскиваем число страниц
	        if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1){
	            $pagecount = intval($matches[1]);
	            break;
	        }
	    }
	    return $pagecount;
	}

    public function reply(Request $request)
    {
		// Получаем и декодируем уведомление
		$data = json_decode($request->getContent());

		switch($data->type)
		{
			case "confirmation":
				// Отвечаем ВК API
				return $this->confirm();

			case "message_new":
				// Передаем задание в очередь
				dispatch(new ReplyToUser($data));

				return "ok";

			default:
				return "ok";
		}
    }
}
